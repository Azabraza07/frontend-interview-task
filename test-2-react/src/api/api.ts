export const fetchData = async (email: string) => {
    return await fetch('http://localhost:4040/endpoint', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email }),
    })
}
