import { Route, Switch, useRouteMatch } from 'react-router'
import StepOne from './StepOne'
import StepTwo from './StepTwo'

const LoginPage = () => {
	const match = useRouteMatch()

	return (
		<Switch>
			<Route path={`${match.path}/step-1`} component={StepOne} />
			<Route path={`${match.path}/step-2`} component={StepTwo} />
		</Switch>
	)
}

export default LoginPage