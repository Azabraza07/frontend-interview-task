import { ChangeEvent, useEffect, useState } from 'react'
import { useHistory } from 'react-router'
import FormInput from '../UI/FormInput/FormInput'
import FormCheckbox from '../UI/FormCheckbox/FormCheckbox'
import Timer from '../components/Timer/Timer'

const StepOne = () => {
	const [email, setEmail] = useState<string>('')
	const [isChecked, setIsChecked] = useState<boolean>(false)
	const [isButtonPressed, setIsButtonPressed] = useState<boolean>(false);
	const history = useHistory()

	useEffect(() => {
		const sessionEmail = sessionStorage.getItem('email')
		if (sessionEmail) {
			setEmail(sessionEmail)
		}
	}, [])

	const handleTimeout = () => {
		history.push('/login/step-2')
		sessionStorage.setItem('email', email)
	};

	const getValidationEmail = (email: string): boolean => {
		const regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
		return regex.test(email)
	}

	const handleChangeInput = (e: ChangeEvent<HTMLInputElement>) => {
		setEmail(e.target.value)
	}
	return (
		<form className='flex flex-col h-full relative' onSubmit={(e) => e.preventDefault()}>
			<FormInput value={email} onChange={handleChangeInput} />
			<div className="p-1"></div>
			<FormCheckbox onChange={(checked) => setIsChecked(checked)} />
			<button
				className="btn btn-primary mt-auto relative top-[280px]"
				onMouseDown={() => setIsButtonPressed(true)}
				onMouseUp={() => setIsButtonPressed(false)}
				onMouseLeave={() => setIsButtonPressed(false)}
				onTouchStart={() => setIsButtonPressed(true)}
				onTouchEnd={() => setIsButtonPressed(false)}
				disabled={!getValidationEmail(email) || !isChecked}>
				Hold to proceed
				<Timer onTimeout={handleTimeout} isButtonPressed={isButtonPressed} />
			</button>

		</form>
	)
}

export default StepOne