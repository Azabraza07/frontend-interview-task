import { useEffect, useState } from 'react'
import FormInput from '../UI/FormInput/FormInput'
import { useHistory } from 'react-router';
import { fetchData } from '../api/api';

const StepPage = () => {
	const [email, setEmail] = useState('')
	const history = useHistory()

	useEffect(() => {
		const saveEmail = sessionStorage.getItem('email');
		if (saveEmail) {
			setEmail(saveEmail);
		}
	}, []);

	const backToPage = () => {
		history.push('/login/step-1')
	}

	const handleChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
		setEmail(e.target.value)
	}

	const handleSubmit = async (e: any) => {
		e.preventDefault();

		try {
			const response = await fetchData(email);
			if (response) {
				const data = response.json()
				console.log('data:', data);
			}
			alert('Success!');
		} catch (error: any) {
			console.error(error.message);
			alert('Please try again.');
		}
	};

	return (
		<form onSubmit={handleSubmit}>
			<FormInput value={email} onChange={handleChangeInput} />
			<div className='flex justify-between items-center flex-wrap'>
				<button className="btn  bg-[#2A323C]  hover:bg-opacity-10 mt-auto relative top-[340px] w-full" onClick={backToPage}>Back</button>
				<button className="btn btn-primary mt-auto relative top-[360px] w-full">Confirm</button>
			</div>
		</form>
	)
}

export default StepPage