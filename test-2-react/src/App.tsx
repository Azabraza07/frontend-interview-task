import { Redirect, Route } from 'react-router'
import LoginPage from './pages/LoginPage'
import Layout from './components/Layout/Layout'

export default function App() {
	return (
		<Layout>
			<Route path="/login" component={LoginPage} />
			<Redirect exact from="/" to="/login/step-1" />
		</Layout>

	)
}




