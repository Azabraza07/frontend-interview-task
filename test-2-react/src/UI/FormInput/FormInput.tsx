interface Props {
	onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
	value?: string;
}

const FormInput: React.FC<Props> = (props) => {

	const { onChange, value } = props

	return (
		<label className="form-control">
			<div className="label">
				<span className="label-text">Email</span>
			</div>
			<input type="text" placeholder="Type here" className="input" value={value} onChange={onChange} />
		</label>
	);
}

export default FormInput;
