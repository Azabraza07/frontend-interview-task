interface Props {
	id: string,
	title: string,
	onClose: () => void
}
const Popup: React.FC<Props> = (props) => {

	const { id, title, onClose } = props

	return (
		<dialog id={id} className="modal">
			<div className="modal-box">
				<form>
					<button className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2" onClick={onClose}>✕</button>
				</form>
				<h3 className="font-bold text-lg">{title}</h3>
			</div>
		</dialog>
	)
}

export default Popup

