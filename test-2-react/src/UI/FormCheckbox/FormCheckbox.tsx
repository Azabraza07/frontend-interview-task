
interface Props {
	onChange: (checked: boolean) => void;
};
const FormCheckbox: React.FC<Props> = (props) => {

	const { onChange } = props

	const handleCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		onChange(event.target.checked);
	};

	return (
		<div className="form-control">
			<label className="label cursor-pointer justify-start gap-2">
				<input type="checkbox" className="checkbox checkbox-primary" onChange={handleCheckboxChange} />
				<span className="label-text">I agree</span>
			</label>
		</div>
	)
}

export default FormCheckbox