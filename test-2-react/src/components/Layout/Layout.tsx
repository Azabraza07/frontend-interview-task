import React, { ReactNode } from 'react'

interface Props {
	children: ReactNode
}

const Layout: React.FC<Props> = (props) => {

	const { children } = props

	return (
		<div className='app'>
			<header className="h-20 bg-primary flex items-center p-4">
				<h1 className="text-3xl text-black">Title</h1>
			</header>
			<main className="flex flex-col p-4 h-full">
				{children}
			</main>
		</div>
	)
}

export default Layout