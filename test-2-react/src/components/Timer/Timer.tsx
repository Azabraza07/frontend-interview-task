import React, { useState, useEffect, useRef } from 'react';
import Spinner from '../../UI/Spinner/Spinner';

interface Props {
	onTimeout: () => void;
	isButtonPressed: boolean;
}

const Timer: React.FC<Props> = (props) => {
	const { isButtonPressed, onTimeout } = props

	const [elapsedTime, setElapsedTime] = useState<number>(0);
	const animationRef = useRef<number | null>(null);
	const startTimeRef = useRef<number | null>(null);

	useEffect(() => {
		if (isButtonPressed) {
			startTimeRef.current = Date.now();

			const animate = () => {
				const currentTime = Date.now() - (startTimeRef.current || 0);
				setElapsedTime(currentTime);

				if (currentTime >= 500) {
					onTimeout();
					return;
				}

				animationRef.current = requestAnimationFrame(animate);
			};

			animationRef.current = requestAnimationFrame(animate);
		} else {
			if (animationRef.current) {
				cancelAnimationFrame(animationRef.current);
			}
			setElapsedTime(0);
			startTimeRef.current = null;
		}

		return () => {
			if (animationRef.current) {
				cancelAnimationFrame(animationRef.current);
			}
		};
	}, [isButtonPressed, onTimeout]);

	if (isButtonPressed) {
		return <Spinner />;
	}

	return null;
};

export default Timer;
